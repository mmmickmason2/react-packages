#!/bin/bash

ROOT_FOLDER=$HOME/Dev

function check_env_variables {
  for var in \
    "GITLAB_PRIVATE_TOKEN" \
    "RUNNER_ID" \
  ; do
    if [ "${!var}" == "" ]; then
      echo "$var is not defined. Check the README for more details."
      exit -1
    fi
  done
}
function prompt {
  if [ "$1" == "" ]; then
    echo "Type the project group"
    read GROUP
  else
    GROUP=$1
  fi

  if [ "$2" == "" ]; then
    echo "Type the project name"
    read NAME
  else
    NAME=$2
  fi

  if [ "$AUTHOR_NAME" == "" ]; then
    if [ "$3" == "" ]; then
      echo "Type author name"
      read AUTHOR_NAME
    else
      AUTHOR_NAME=$3
    fi
  fi

  if [ "$AUTHOR_EMAIL" == "" ]; then
    if [ "$4" == "" ]; then
      echo "Type author email"
      read AUTHOR_EMAIL
    else
      AUTHOR_EMAIL=$4
    fi
  fi

  echo "Type the project description"
  read DESCRIPTION
}
function set_variables {
  FILE=$1
  CLASS=$(echo $NAME | sed -r 's/(^|-)([a-z])/\U\2/g')
  FUNCTION=$(echo ${CLASS,})
  sed -i \
    -e "s/<GROUP>/$GROUP/g" \
    -e "s/<NAME>/$NAME/g" \
    -e "s/<AUTHOR_NAME>/$AUTHOR_NAME/g" \
    -e "s/<AUTHOR_EMAIL>/$AUTHOR_EMAIL/g" \
    -e "s/<DESCRIPTION>/$DESCRIPTION/g" \
    -e "s/<CLASS>/$CLASS/g" \
    -e "s/<FUNCTION>/$FUNCTION/g" \
    $ROOT_FOLDER/$GROUP/$NAME/$FILE
}
function create_folders {
  for folder in \
    "src" \
    "public" \
  ; do
    echo "create the folder $folder"
    mkdir -p $ROOT_FOLDER/$GROUP/$NAME/$folder
  done
}
function simple_copies {
  for file in \
    ".env" \
    ".eslintignore" \
    ".eslintrc.yml" \
    ".gitignore" \
    ".gitlab-ci.yml" \
    "config-overrides.js" \
    "package.json" \
    "README.md" \
    "LICENSE" \
    "public/favicon.ico" \
    "public/index.html" \
    "public/manifest.json" \
    "src/index.js" \
    "src/index.css" \
    "src/App.js" \
    "src/App.css" \
    "src/registerServiceWorker.js" \
  ; do
    echo "copy $file"
    cp ./scripts/project-template/$file $ROOT_FOLDER/$GROUP/$NAME/$file
    set_variables $file
  done
}
function change_directory {
  echo "change directory to $ROOT_FOLDER/$GROUP/$NAME"
  cd $ROOT_FOLDER/$GROUP/$NAME
}
function get_namespace_id {
  echo "get namespace id"
  export GROUP_NAMESPACE_ID=$(curl -s -X GET --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" https://gitlab.com/api/v4/namespaces?search=${GROUP} | jq '.[0].id')
  echo "GROUP_NAMESPACE_ID = $GROUP_NAMESPACE_ID"
}
function create_git_repository {
  echo "create gitlab repository"
  curl -s -X POST --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" \
    --form "namespace_id=$GROUP_NAMESPACE_ID" \
    --form "visibility=private" \
    --form "path=$NAME" \
    --form "description=$DESCRIPTION ([$NAME](http://$NAME.ws3.4ge.it))" \
    https://gitlab.com/api/v4/projects > /dev/null
}
function enable_runner {
  echo "enable the CI runner $RUNNER_ID"
  curl -s -X POST --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" \
    --form "runner_id=$RUNNER_ID" \
    https://gitlab.com/api/v4/projects/$GROUP%2F$NAME/runners > /dev/null
}
function git_actions {
  echo "git init"
  git init > /dev/null

  echo "add remote repository URL"
  git remote add origin git@gitlab.com:$GROUP/$NAME.git

  echo "git add"
  git add .

  echo "git commit"
  git commit -m "feat(init) new repository" > /dev/null

  echo "git push"
  git push -u origin master > /dev/null
}
function usage {
  echo
  echo
  echo "The new repository has been properly generated and is available at https://gitlab.com/$GROUP/$NAME"
  echo
  echo "You can clone the repository with the following command:"
  echo
  echo "git clone git@gitlab.com:$GROUP/$NAME.git"
  echo
  echo "Or just get access to the locally generated folder:"
  echo
  echo "cd $ROOT_FOLDER/$GROUP/$NAME"
  echo
}

check_env_variables
prompt $*
create_folders
simple_copies
change_directory
get_namespace_id
create_git_repository
enable_runner
git_actions
usage
