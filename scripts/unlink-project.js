'use strict'

const path = require('path')
const chalk = require('chalk')
const { execSync } = require('child_process')
const getPackages = require('./_getPackages')

const PACKAGES_DIR = path.resolve(__dirname, '../packages')

if (process.argv.length < 3) {
  console.log(chalk.red('Project dir missing'))
  process.exit(-1)
}

const projectDir = process.argv[2]

console.log(chalk.cyan(`Remove packages links from the project located at ${projectDir}`))

function getPackageName(file) {
  return path.relative(PACKAGES_DIR, file).split(path.sep)[0];
}

getPackages().forEach(p => {
  const pkgName = getPackageName(p)
  console.log(chalk.cyan(`Unlink @4geit/${pkgName}`))
  execSync(`yarn unlink @4geit/${pkgName}`, { stdio: [0, 1, 2], cwd: projectDir })
})
