'use strict'

const chalk = require('chalk')
const { execSync } = require('child_process')
const getPackages = require('./_getPackages')

getPackages().forEach(p => {
  console.log(chalk.cyan(`Unlink ${p}`))
  execSync('yarn unlink', { stdio: [0, 1, 2], cwd: p })
})
