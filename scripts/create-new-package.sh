#!/bin/bash

function prompt {
  if [ "$1" == "" ]; then
    echo "Type the package name"
    read NAME
  else
    NAME=$1
  fi

  if [ "$2" == "" ]; then
    echo "Type the package type <component|store>"
    read TYPE
  else
    TYPE=$2
  fi

  if [ "$AUTHOR_NAME" == "" ]; then
    if [ "$3" == "" ]; then
      echo "Type author name"
      read AUTHOR_NAME
    else
      AUTHOR_NAME=$3
    fi
  fi

  if [ "$AUTHOR_EMAIL" == "" ]; then
    if [ "$4" == "" ]; then
      echo "Type author email"
      read AUTHOR_EMAIL
    else
      AUTHOR_EMAIL=$4
    fi
  fi

  echo "Type the package description"
  read DESCRIPTION
}
function set_variables {
  FILE=$1
  CLASS=$(echo rct-$NAME-$TYPE | sed -r 's/(^|-)([a-z])/\U\2/g')
  FUNCTION=$(echo ${CLASS,})
  NAME_CLASS=$(echo $NAME | sed -r 's/(^|-)([a-z])/\U\2/g')
  NAME_FUNCTION=$(echo ${NAME_CLASS,})
  NAME_TYPE_CLASS=$(echo $NAME-$TYPE | sed -r 's/(^|-)([a-z])/\U\2/g')
  NAME_TYPE_FUNCTION=$(echo ${NAME_TYPE_CLASS,})
  sed -i \
    -e "s/<NAME>/$NAME/g" \
    -e "s/<TYPE>/$TYPE/g" \
    -e "s/<AUTHOR_NAME>/$AUTHOR_NAME/g" \
    -e "s/<AUTHOR_EMAIL>/$AUTHOR_EMAIL/g" \
    -e "s/<DESCRIPTION>/$DESCRIPTION/g" \
    -e "s/<CLASS>/$CLASS/g" \
    -e "s/<FUNCTION>/$FUNCTION/g" \
    -e "s/<NAME_CLASS>/$NAME_CLASS/g" \
    -e "s/<NAME_FUNCTION>/$NAME_FUNCTION/g" \
    -e "s/<NAME_TYPE_CLASS>/$NAME_TYPE_CLASS/g" \
    -e "s/<NAME_TYPE_FUNCTION>/$NAME_TYPE_FUNCTION/g" \
    ./packages/rct-$NAME-$TYPE/$FILE
}
function create_folders {
  for folder in \
    "rct-$NAME-$TYPE" \
    "rct-$NAME-$TYPE/src" \
  ; do
    echo "create the folder $folder"
    mkdir -p packages/$folder
  done
}
function simple_copies {
  for file in \
    ".npmignore" \
  ; do
    echo "copy $file"
    cp ./scripts/package-template/$file ./packages/rct-$NAME-$TYPE/$file
    set_variables $file
  done
}
function simple_type_copies {
  for file in \
    "src/index.js" \
    "README.md" \
    "package.json" \
  ; do
    echo "copy $file"
    cp ./scripts/package-template/$file.$TYPE ./packages/rct-$NAME-$TYPE/$file
    set_variables $file
  done
}
function copy_type_files {
  for extension in \
    "js" \
    "test.js" \
    "stories.js" \
  ; do
    echo "copy src/rct-$NAME.$TYPE.$extension"
    cp ./scripts/package-template/src/$TYPE.$extension ./packages/rct-$NAME-$TYPE/src/rct-$NAME.$TYPE.$extension
    set_variables src/rct-$NAME.$TYPE.$extension
  done
  if [ "$TYPE" == "component" ]; then
    for extension in \
      "css" \
    ; do
      echo "copy src/rct-$NAME.$TYPE.$extension"
      cp ./scripts/package-template/src/$TYPE.$extension ./packages/rct-$NAME-$TYPE/src/rct-$NAME.$TYPE.$extension
      set_variables src/rct-$NAME.$TYPE.$extension
    done
  fi
}
function run_yarn {
  echo "link packages to each other (yarn)"
  yarn
}
function usage {
  echo
  echo
  echo "The new package has been properly generated and is available under the packages folder."
  echo
  echo "You can get access to the generated package with the following command:"
  echo
  echo "cd packages/rct-$NAME-$TYPE"
  echo
}

prompt $*
create_folders
simple_copies
simple_type_copies
copy_type_files
run_yarn
usage
