// eslint-disable-next-line import/no-extraneous-dependencies
import rctSwaggerClientStore from '@4geit/rct-swagger-client-store'

it('test', async () => {
  await rctSwaggerClientStore.buildClient({})
  await rctSwaggerClientStore.buildClientWithToken({})
  await rctSwaggerClientStore.client.apis.Account.account({})
})
