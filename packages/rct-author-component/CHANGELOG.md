# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.97.4"></a>
## [1.97.4](https://gitlab.com/4geit/react-packages/compare/v1.97.3...v1.97.4) (2017-12-08)


### Bug Fixes

* **landing-page-layout:** fix layout ([03e3d1c](https://gitlab.com/4geit/react-packages/commit/03e3d1c))




<a name="1.97.3"></a>
## [1.97.3](https://gitlab.com/4geit/react-packages/compare/v1.97.2...v1.97.3) (2017-12-07)


### Bug Fixes

* **author-component:** fix layout issue ([867362e](https://gitlab.com/4geit/react-packages/commit/867362e))




<a name="1.97.2"></a>
## [1.97.2](https://gitlab.com/4geit/react-packages/compare/v1.97.1...v1.97.2) (2017-12-07)




**Note:** Version bump only for package @4geit/rct-author-component
