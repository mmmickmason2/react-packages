import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { observer } from 'mobx-react'

import classNames from 'classnames'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import Drawer from 'material-ui/Drawer'
import Divider from 'material-ui/Divider'
import MenuIcon from 'material-ui-icons/Menu'
import IconButton from 'material-ui/IconButton'
import ChevronRightIcon from 'material-ui-icons/ChevronRight'
import Tooltip from 'material-ui/Tooltip'

import './rct-right-side-menu.component.css'

const debug = buildDebug('react-packages:packages:rct-right-side-menu-component')

@withStyles(theme => ({
  root: {
    [theme.breakpoints.down('lg')]: {
      width: '900px',
    },
    [theme.breakpoints.up('lg')]: {
      width: '1200px',
    },
    margin: 'auto',
    padding: '0 20px',
    height: '100vh',
    zIndex: 1,
  },
  appFrame: {
    position: 'relative',
    display: 'flex',
    width: '100%',
    height: '100%',
  },
  drawerWidth: {
    width: 600,
  },
  drawerPaper: {
    position: 'fixed',
    height: '100vh',
    marginTop: '68px',
    overflowX: 'hidden',
  },
  drawerPaperClose: {
    width: 0,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  drawerInner: {
    // Make the items inside not wrap when transitioning:
  },
  drawerInnerInner: {
    paddingLeft: 20,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    height: 56,
    [theme.breakpoints.up('sm')]: {
      height: 64,
    },
  },
  content: {
    width: '100%',
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    height: '100vh',
    [theme.breakpoints.up('sm')]: {
      content: {
        height: '100vh',
      },
    },
  },
  contentShift: {
    marginLeft: 0,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    right: 0,
    position: 'fixed',
    paddingLeft: 12,
    marginRight: 20,
  },
  hide: {
    display: 'none',
  },
  // TBD
}))
@withWidth()
// @inject('xyzStore')
@observer
export default class RctRightSideMenuComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.any.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    contentComponent: PropTypes.any,
    enabled: PropTypes.bool,
    open: PropTypes.bool,
    openButtonTitle: PropTypes.string,
    closeButtonTitle: PropTypes.string,
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any.isRequired,
  }
  static defaultProps = {
    contentComponent: undefined,
    enabled: true,
    open: false,
    openButtonTitle: 'Open',
    closeButtonTitle: 'Close',
  }

  constructor(props) {
    super(props)
    this.state = {
      open: props.open,
    }
  }

  handleDrawerOpen = () => {
    debug('handleDrawerOpen()')
    this.setState({ open: true })
  }
  handleDrawerClose = () => {
    debug('handleDrawerClose()')
    this.setState({ open: false })
  }

  render() {
    debug('render()')
    const {
      classes, children, contentComponent, enabled,
      openButtonTitle, closeButtonTitle,
    } = this.props
    const {
      root, appFrame, content, contentShift, menuButton, hide, drawerWidth,
      drawerPaper, drawerPaperClose, drawerInner, drawerHeader, drawerInnerInner,
    } = classes
    return (
      <div className={root} >
        <div className={appFrame} >
          <main className={classNames(content, this.state.open && contentShift)} >
            { contentComponent }
          </main>
          { enabled && (
            <Tooltip title={openButtonTitle}>
              <IconButton
                onClick={this.handleDrawerOpen}
                className={classNames(menuButton, this.state.open && hide)}
              >
                <MenuIcon />
              </IconButton>
            </Tooltip>
          ) }
          { enabled && (
            <Drawer
              type="persistent"
              anchor="right"
              open={this.state.open}
              classes={{
                paper: classNames(drawerWidth, drawerPaper, !this.state.open && drawerPaperClose),
              }}
            >
              <div className={classNames(drawerWidth, drawerInner)} >
                <div className={drawerHeader} >
                  <Tooltip title={closeButtonTitle}>
                    <IconButton onClick={this.handleDrawerClose} >
                      <ChevronRightIcon />
                    </IconButton>
                  </Tooltip>
                </div>
                <Divider />
                <div className={drawerInnerInner} >
                  { children }
                </div>
              </div>
            </Drawer>
          ) }
        </div>
      </div>
    )
  }
}
