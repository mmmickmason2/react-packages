import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { Link, withRouter } from 'react-router-dom'
import { observable, action, runInAction } from 'mobx'
import { inject, observer } from 'mobx-react'
import Autosuggest from 'react-autosuggest'
import match from 'autosuggest-highlight/match'
import parse from 'autosuggest-highlight/parse'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import IconButton from 'material-ui/IconButton'
import SearchIcon from 'material-ui-icons/Search'
import Grid from 'material-ui/Grid'
import Tooltip from 'material-ui/Tooltip'
import TextField from 'material-ui/TextField'
import { MenuItem } from 'material-ui/Menu'
import Paper from 'material-ui/Paper'

import './rct-search-input.component.css'

const debug = buildDebug('react-packages:packages:rct-search-input-component')

export const SearchInputSource = () => {}
SearchInputSource.propTypes = {
  listOperationId: PropTypes.string.isRequired,
  field: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
}

function renderInput(inputProps) {
  debug('renderInput()')
  const {
    classes, autoFocus, value, ref, ...other
  } = inputProps
  return (
    <TextField
      autoFocus={autoFocus}
      fullWidth
      className={classes.textField}
      value={value}
      inputRef={ref}
      InputProps={{
        classes: {
          input: classes.input,
        },
        ...other,
      }}
    />
  )
}
const renderSuggestion = (suggestion, { query, isHighlighted }) => {
  debug('renderSuggestion()')
  const matches = match(suggestion.label, query)
  const parts = parse(suggestion.label, matches)
  /* eslint-disable react/no-array-index-key */
  debug(suggestion)
  return (
    <MenuItem selected={isHighlighted} component="div">
      <div>
        { parts.map((part, index) => (
          part.highlight
            ? (
              <Link
                key={index}
                to={suggestion.link}
                style={{
                  textDecoration: 'none',
                  color: 'rgba(0, 0, 0, 0.87)',
                }}
              >
                <span style={{ fontWeight: 300 }}>{ part.text }</span>
              </Link>
            )
            : (
              <Link
                key={index}
                to={suggestion.link}
                style={{
                  textDecoration: 'none',
                  color: 'rgba(0, 0, 0, 0.87)',
                }}
              >
                <strong style={{ fontWeight: 500 }}>{ part.text }</strong>
              </Link>
            )
        )) }
      </div>
    </MenuItem>
  )
  /* eslint-enable react/no-array-index-key */
}
function renderSuggestionsContainer(options) {
  debug('renderSuggestionsContainer()')
  const { containerProps, children } = options
  return (
    <Paper {...containerProps} square>
      { children }
    </Paper>
  )
}
const getSuggestionValue = (suggestion) => {
  debug('getSuggestionValue()')
  return suggestion.label
}

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  container: {
    flexGrow: 1,
    position: 'relative',
    // height: 200,
  },
  suggestionsContainerOpen: {
    position: 'absolute',
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit * 3,
    left: 0,
    right: 0,
  },
  suggestion: {
    display: 'block',
  },
  suggestionsList: {
    margin: 0,
    padding: 0,
    listStyleType: 'none',
  },
  textField: {
    width: '100%',
  },
  input: {
    color: 'white',
  },
}))
@withWidth()
@withRouter
@inject('swaggerClientStore', 'notificationStore')
@observer
export default class RctSearchInputComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.any.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any.isRequired,
  }
  static defaultProps = {
    // TBD
  }

  @observable data = []
  @observable value = ''

  @action async fetchData({
    listOperationId, field, link, value,
  }) {
    debug('fetchData()')
    const {
      swaggerClientStore, notificationStore,
    } = this.props
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      const { body } = await Account[listOperationId]({
        [field]: value,
        pageSize: 5,
      })
      runInAction(() => {
        if (body.length) {
          debug(body)
          body.forEach((item) => {
            this.data.push({
              label: item[field],
              listOperationId,
              link,
            })
          })
        }
      })
    } catch (err) {
      debug(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }

  handleFetchRequested = action(async ({ value }) => {
    debug('handleFetchRequested()')
    debug(value)
    this.data = []
    const { children } = this.props
    await Promise.all(await children.map(async ({ props: { listOperationId, field, link } }) => {
      await this.fetchData({
        listOperationId,
        field,
        link,
        value,
      })
    }))
  })
  @action handleClearRequested = () => {
    debug('handleClearRequested()')
    this.data = []
  }
  @action handleChange = (event, { newValue }) => {
    debug('handleChange()')
    debug(newValue)
    this.value = newValue
  }

  render() {
    debug('render()')
    const { classes } = this.props
    // create a copy
    let data = this.data.slice()
    // remove mobx object
    data = data.map(({ label, listOperationId, link }) => ({ label, listOperationId, link }))
    // remove duplicates
    data = Array.from(new Set(data))
    debug(data)
    return (
      <Grid
        container
        alignItems="center"
        style={{
          height: '30px',
        }}
      >
        <Grid item xs>
          <Autosuggest
            theme={{
              container: classes.container,
              suggestionsContainerOpen: classes.suggestionsContainerOpen,
              suggestionsList: classes.suggestionsList,
              suggestion: classes.suggestion,
            }}
            renderInputComponent={renderInput}
            suggestions={data}
            onSuggestionsFetchRequested={this.handleFetchRequested}
            onSuggestionsClearRequested={this.handleClearRequested}
            renderSuggestionsContainer={renderSuggestionsContainer}
            getSuggestionValue={getSuggestionValue}
            renderSuggestion={renderSuggestion}
            inputProps={{
              autoFocus: true,
              classes,
              placeholder: 'Search',
              value: this.value,
              onChange: this.handleChange,
            }}
          />
        </Grid>
        <Grid item>
          <Tooltip title="Search">
            <IconButton color="contrast">
              <SearchIcon />
            </IconButton>
          </Tooltip>
        </Grid>
      </Grid>
    )
  }
}
