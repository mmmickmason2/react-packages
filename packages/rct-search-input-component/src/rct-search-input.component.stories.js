/* eslint-disable react/no-multi-comp */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Provider, inject, observer } from 'mobx-react'
import { BrowserRouter } from 'react-router-dom'
import { storiesOf } from '@storybook/react'
import Grid from 'material-ui/Grid'
import blue from 'material-ui/colors/blue'
import centered from '@storybook/addon-centered'

import commonStore from '@4geit/rct-common-store'
import swaggerClientStore from '@4geit/rct-swagger-client-store'
import notificationStore from '@4geit/rct-notification-store'

import RctSearchInputComponent, { SearchInputSource } from './rct-search-input.component'

const stores = {
  commonStore,
  swaggerClientStore,
  notificationStore,
}

class ErrorBoundary extends React.Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = { hasError: false }
  }

  // eslint-disable-next-line no-unused-vars
  componentDidCatch(error, info) {
    // Display fallback UI
    this.setState({ hasError: true })
    // You can also log the error to an error reporting service
    // logErrorToMyService(error, info)
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return (
        <h1>Something went wrong.</h1>
      )
    }
    return this.props.children
  }
}

@inject('swaggerClientStore', 'commonStore')
@observer
class App extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any.isRequired,
  }

  async componentWillMount() {
    try {
      await swaggerClientStore.buildClient({
        apiUrl: process.env.STORYBOOK_API_URL || 'http://localhost:10010/v1',
      })
      await swaggerClientStore.buildClientWithToken({
        token: process.env.STORYBOOK_TOKEN,
      })
      const {
        body: {
          token, _id, id, ...fields
        },
      } = await swaggerClientStore.client.apis.Account.account()
      commonStore.setToken(token)
      commonStore.setUser({ ...fields })
      commonStore.setAppLoaded()
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err)
    }
  }

  render() {
    const { children } = this.props
    if (commonStore.appLoaded) {
      return (
        <div>
          { children }
        </div>
      )
    }
    return (
      <div>Loading...</div>
    )
  }
}

storiesOf('RctSearchInputComponent', module)
  .add('simple usage', () => (
    <Provider {...stores}>
      <App>
        <ErrorBoundary>
          <Grid
            container
            justify="flex-end"
            alignItems="center"
            style={{
              backgroundColor: blue[500],
              height: 100,
            }}
          >
            <Grid item>
              <RctSearchInputComponent>
                <SearchInputSource listOperationId="productList" field="name" />
                <SearchInputSource listOperationId="contactList" field="firstname" />
              </RctSearchInputComponent>
            </Grid>
          </Grid>
        </ErrorBoundary>
      </App>
    </Provider>
  ))
  .addDecorator(centered)
  .add('search-bar alone', () => (
    <BrowserRouter>
      <Provider {...stores}>
        <App>
          <ErrorBoundary>
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="center"
              style={{
                backgroundColor: blue[500],
                width: 420,
                height: 100,
                padding: 20,
              }}
            >
              <Grid item xs>
                <RctSearchInputComponent>
                  <SearchInputSource listOperationId="productList" field="name" link="/products" />
                  <SearchInputSource listOperationId="contactList" field="firstname" link="/contacts" />
                </RctSearchInputComponent>
              </Grid>
            </Grid>
          </ErrorBoundary>
        </App>
      </Provider>
    </BrowserRouter>
  ))
