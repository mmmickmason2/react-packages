# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.92.0"></a>
# [1.92.0](https://gitlab.com/4geit/react-packages/compare/v1.91.0...v1.92.0) (2017-12-01)




**Note:** Version bump only for package @4geit/rct-project-component

<a name="1.89.1"></a>
## [1.89.1](https://gitlab.com/4geit/react-packages/compare/v1.89.0...v1.89.1) (2017-11-29)




**Note:** Version bump only for package @4geit/rct-project-component

<a name="1.89.0"></a>
# [1.89.0](https://gitlab.com/4geit/react-packages/compare/v1.88.0...v1.89.0) (2017-11-29)




**Note:** Version bump only for package @4geit/rct-project-component

<a name="1.87.5"></a>
## [1.87.5](https://gitlab.com/4geit/react-packages/compare/v1.87.4...v1.87.5) (2017-11-24)




**Note:** Version bump only for package @4geit/rct-project-component

<a name="1.87.1"></a>
## [1.87.1](https://gitlab.com/4geit/react-packages/compare/v1.87.0...v1.87.1) (2017-11-23)




**Note:** Version bump only for package @4geit/rct-project-component

<a name="1.86.3"></a>
## [1.86.3](https://gitlab.com/4geit/react-packages/compare/v1.86.2...v1.86.3) (2017-11-21)




**Note:** Version bump only for package @4geit/rct-project-component

<a name="1.86.2"></a>
## [1.86.2](https://gitlab.com/4geit/react-packages/compare/v1.86.1...v1.86.2) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-project-component

<a name="1.86.0"></a>
# [1.86.0](https://gitlab.com/4geit/react-packages/compare/v1.85.3...v1.86.0) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-project-component

<a name="1.85.3"></a>
## [1.85.3](https://gitlab.com/4geit/react-packages/compare/v1.85.2...v1.85.3) (2017-11-19)




**Note:** Version bump only for package @4geit/rct-project-component

<a name="1.85.2"></a>
## [1.85.2](https://gitlab.com/4geit/react-packages/compare/v1.85.1...v1.85.2) (2017-11-18)




**Note:** Version bump only for package @4geit/rct-project-component

<a name="1.83.5"></a>
## [1.83.5](https://gitlab.com/4geit/react-packages/compare/v1.83.4...v1.83.5) (2017-11-06)




**Note:** Version bump only for package @4geit/rct-project-component

<a name="1.83.3"></a>
## [1.83.3](https://gitlab.com/4geit/react-packages/compare/v1.83.2...v1.83.3) (2017-11-04)




**Note:** Version bump only for package @4geit/rct-project-component

<a name="1.83.2"></a>
## [1.83.2](https://gitlab.com/4geit/react-packages/compare/v1.83.1...v1.83.2) (2017-11-04)




**Note:** Version bump only for package @4geit/rct-project-component

<a name="1.82.4"></a>
## [1.82.4](https://gitlab.com/4geit/react-packages/compare/v1.82.3...v1.82.4) (2017-11-02)




**Note:** Version bump only for package @4geit/rct-project-component

<a name="1.82.1"></a>
## [1.82.1](https://gitlab.com/4geit/react-packages/compare/v1.82.0...v1.82.1) (2017-11-01)


### Bug Fixes

* **storybook:** fix remaining issues ([b29000d](https://gitlab.com/4geit/react-packages/commit/b29000d))




<a name="1.81.2"></a>
## [1.81.2](https://gitlab.com/4geit/react-packages/compare/v1.81.1...v1.81.2) (2017-10-31)




**Note:** Version bump only for package @4geit/rct-project-component

<a name="1.80.2"></a>
## [1.80.2](https://gitlab.com/4geit/react-packages/compare/v1.80.1...v1.80.2) (2017-10-29)




**Note:** Version bump only for package @4geit/rct-project-component

<a name="1.78.0"></a>
# [1.78.0](https://gitlab.com/4geit/react-packages/compare/v1.77.0...v1.78.0) (2017-10-21)


### Features

* **test:** add test environment with jest ([34a9b47](https://gitlab.com/4geit/react-packages/commit/34a9b47))




<a name="1.52.0"></a>
# [1.52.0](https://gitlab.com/4geit/react-packages/compare/v1.51.2...v1.52.0) (2017-10-04)


### Features

* **chatbox-grid:** add fetchMaximizedItem to store and use within component ([e1df4e4](https://gitlab.com/4geit/react-packages/commit/e1df4e4))




<a name="1.46.0"></a>
# [1.46.0](https://gitlab.com/4geit/react-packages/compare/v1.45.0...v1.46.0) (2017-10-03)




**Note:** Version bump only for package @4geit/rct-project-component

<a name="1.32.2"></a>
## [1.32.2](https://gitlab.com/4geit/react-packages/compare/v1.32.1...v1.32.2) (2017-09-18)




**Note:** Version bump only for package @4geit/rct-project-component

<a name="1.15.4"></a>
## [1.15.4](https://gitlab.com/4geit/react-packages/compare/v1.15.2...v1.15.4) (2017-09-06)


### Bug Fixes

* **project:** remove deprecated prop, add header component dep ([1d07e3e](https://gitlab.com/4geit/react-packages/commit/1d07e3e))




<a name="1.15.3"></a>
## [1.15.3](https://gitlab.com/4geit/react-packages/compare/v1.15.2...v1.15.3) (2017-09-05)


### Bug Fixes

* **project:** remove deprecated prop, add header component dep ([1d07e3e](https://gitlab.com/4geit/react-packages/commit/1d07e3e))




<a name="1.15.2"></a>
## [1.15.2](https://gitlab.com/4geit/react-packages/compare/v1.15.0...v1.15.2) (2017-09-05)


### Bug Fixes

* **project:** removed overlapping components from project ([806ca9b](https://gitlab.com/4geit/react-packages/commit/806ca9b))




<a name="1.15.1"></a>
## [1.15.1](https://gitlab.com/4geit/react-packages/compare/v1.15.0...v1.15.1) (2017-09-05)


### Bug Fixes

* **project:** removed overlapping components from project ([806ca9b](https://gitlab.com/4geit/react-packages/commit/806ca9b))




<a name="1.15.0"></a>
# [1.15.0](https://gitlab.com/4geit/react-packages/compare/v1.14.0...v1.15.0) (2017-09-04)


### Features

* **project:** improve project component ([ff95e5e](https://gitlab.com/4geit/react-packages/commit/ff95e5e))




<a name="1.14.0"></a>
# [1.14.0](https://gitlab.com/4geit/react-packages/compare/v1.12.0...v1.14.0) (2017-09-04)


### Bug Fixes

* **lerna:** fix versioning issue ([eee69c7](https://gitlab.com/4geit/react-packages/commit/eee69c7))


### Features

* **project component:** add main project component ([c9a4c36](https://gitlab.com/4geit/react-packages/commit/c9a4c36))




<a name="0.1.0"></a>
# [0.1.0](https://gitlab.com/4geit/react-packages/compare/v1.12.0...v0.1.0) (2017-09-04)


### Features

* **project component:** add main project component ([c9a4c36](https://gitlab.com/4geit/react-packages/commit/c9a4c36))
