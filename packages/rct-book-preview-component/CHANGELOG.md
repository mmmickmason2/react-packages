# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.97.4"></a>
## [1.97.4](https://gitlab.com/4geit/react-packages/compare/v1.97.3...v1.97.4) (2017-12-08)


### Bug Fixes

* **landing-page-layout:** fix layout ([03e3d1c](https://gitlab.com/4geit/react-packages/commit/03e3d1c))




<a name="1.95.0"></a>
# [1.95.0](https://gitlab.com/4geit/react-packages/compare/v1.94.0...v1.95.0) (2017-12-05)


### Features

* **landing-page-layout-component:** completions ([1dd57da](https://gitlab.com/4geit/react-packages/commit/1dd57da))




<a name="1.93.0"></a>
# [1.93.0](https://gitlab.com/4geit/react-packages/compare/v1.92.1...v1.93.0) (2017-12-05)


### Features

* **book preview:** add link to sample page ([212587a](https://gitlab.com/4geit/react-packages/commit/212587a))
* **book preview:** add the link `en savoir +` that scrolls the page down to the next section of the ([2399e96](https://gitlab.com/4geit/react-packages/commit/2399e96))
* **Book preview link:** Link to book samples as a prop ([5ba960b](https://gitlab.com/4geit/react-packages/commit/5ba960b))




<a name="1.91.0"></a>
# [1.91.0](https://gitlab.com/4geit/react-packages/compare/v1.90.0...v1.91.0) (2017-12-01)


### Features

* **background image:** minor changes ([2de2a40](https://gitlab.com/4geit/react-packages/commit/2de2a40))
* **background image:** minor fix ([5111529](https://gitlab.com/4geit/react-packages/commit/5111529))
* **book preview:** add black image background ([8ec0ad6](https://gitlab.com/4geit/react-packages/commit/8ec0ad6))
* **book preview:** added text under button for link ([3015480](https://gitlab.com/4geit/react-packages/commit/3015480))
* **book preview:** background image dynamic ([c471060](https://gitlab.com/4geit/react-packages/commit/c471060))
* **book preview:** fixed button, image and padding ([a31aa4f](https://gitlab.com/4geit/react-packages/commit/a31aa4f))
* **book preview:** last fix ([e7e83e3](https://gitlab.com/4geit/react-packages/commit/e7e83e3))
* **book preview:** minor fix ([7c8445a](https://gitlab.com/4geit/react-packages/commit/7c8445a))
* **check item class:** create check-item file ([87f7ec0](https://gitlab.com/4geit/react-packages/commit/87f7ec0))
* **FormControlLabel fix:** Classes fixing ([bc0b584](https://gitlab.com/4geit/react-packages/commit/bc0b584))
* **Styling:** Class Check item and book preview component styling fix ([2f4426a](https://gitlab.com/4geit/react-packages/commit/2f4426a))




<a name="1.89.1"></a>
## [1.89.1](https://gitlab.com/4geit/react-packages/compare/v1.89.0...v1.89.1) (2017-11-29)




**Note:** Version bump only for package @4geit/rct-book-preview-component

<a name="1.89.0"></a>
# [1.89.0](https://gitlab.com/4geit/react-packages/compare/v1.88.0...v1.89.0) (2017-11-29)


### Features

* **book preview:** css ([4556189](https://gitlab.com/4geit/react-packages/commit/4556189))
* **book preview:** grid and list fix ([cb37eb5](https://gitlab.com/4geit/react-packages/commit/cb37eb5))
* **book preview:** layout ([8ebc8a8](https://gitlab.com/4geit/react-packages/commit/8ebc8a8))
* **book preview:** list fix ([77ce730](https://gitlab.com/4geit/react-packages/commit/77ce730))
* **book preview:** minor fix ([53fe017](https://gitlab.com/4geit/react-packages/commit/53fe017))
* **book preview:** Minor fix ([8bc4b5f](https://gitlab.com/4geit/react-packages/commit/8bc4b5f))
* **book preview:** moving List ([924bd3a](https://gitlab.com/4geit/react-packages/commit/924bd3a))
* **Book preview component:** make the checkbox list dynamic ([3f40ffc](https://gitlab.com/4geit/react-packages/commit/3f40ffc))




<a name="1.88.0"></a>
# [1.88.0](https://gitlab.com/4geit/react-packages/compare/v1.87.5...v1.88.0) (2017-11-24)


### Features

* **Book preview component:** make the checkbox list dynamic ([3590259](https://gitlab.com/4geit/react-packages/commit/3590259))




<a name="1.87.5"></a>
## [1.87.5](https://gitlab.com/4geit/react-packages/compare/v1.87.4...v1.87.5) (2017-11-24)




**Note:** Version bump only for package @4geit/rct-book-preview-component

<a name="1.86.2"></a>
## [1.86.2](https://gitlab.com/4geit/react-packages/compare/v1.86.1...v1.86.2) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-book-preview-component

<a name="1.86.0"></a>
# [1.86.0](https://gitlab.com/4geit/react-packages/compare/v1.85.3...v1.86.0) (2017-11-20)


### Bug Fixes

* **book-preview:** minor fix ([e75e197](https://gitlab.com/4geit/react-packages/commit/e75e197))


### Features

* **Book preview component:** add grid with elements ([a8e9f5b](https://gitlab.com/4geit/react-packages/commit/a8e9f5b))




<a name="1.85.3"></a>
## [1.85.3](https://gitlab.com/4geit/react-packages/compare/v1.85.2...v1.85.3) (2017-11-19)




**Note:** Version bump only for package @4geit/rct-book-preview-component

<a name="1.85.2"></a>
## [1.85.2](https://gitlab.com/4geit/react-packages/compare/v1.85.1...v1.85.2) (2017-11-18)




**Note:** Version bump only for package @4geit/rct-book-preview-component

<a name="1.85.0"></a>
# [1.85.0](https://gitlab.com/4geit/react-packages/compare/v1.84.3...v1.85.0) (2017-11-16)


### Features

* **Created component:** new package ([37686e8](https://gitlab.com/4geit/react-packages/commit/37686e8))
