import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Provider, inject, observer } from 'mobx-react'
import { BrowserRouter } from 'react-router-dom'
import { storiesOf } from '@storybook/react'
import centered from '@storybook/addon-centered'
import { withInfo } from '@storybook/addon-info'

import commonStore from '@4geit/rct-common-store'
import swaggerClientStore from '@4geit/rct-swagger-client-store'

import RctBookPreviewComponent from './rct-book-preview.component'
import CheckItem from './check-item'

import fbCover from './fbCover.png'
import background from './background.jpg'

const stores = {
  commonStore,
  swaggerClientStore,
}

@inject('swaggerClientStore', 'commonStore')
@observer
class App extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any.isRequired,
  }

  async componentWillMount() {
    try {
      await swaggerClientStore.buildClient({
        apiUrl: process.env.STORYBOOK_API_URL || 'http://localhost:10010/v1',
      })
      await swaggerClientStore.buildClientWithToken({
        token: process.env.STORYBOOK_TOKEN,
      })
      const {
        body: {
          token, _id, id, ...fields
        },
      } = await swaggerClientStore.client.apis.Account.account()
      commonStore.setToken(token)
      commonStore.setUser({ ...fields })
      commonStore.setAppLoaded()
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err)
    }
  }

  render() {
    const { children } = this.props
    if (commonStore.appLoaded) {
      return (
        <div>
          { children }
        </div>
      )
    }
    return (
      <div>Loading...</div>
    )
  }
}

storiesOf('RctBookPreviewComponent', module)
  .addDecorator(centered)
  .add('simple usage', withInfo()(() => (
    <BrowserRouter>
      <Provider {...stores}>
        <App>
          <RctBookPreviewComponent
            backgroundImage={background}
            button="Commandez le vôtre !"
            title="Le Flash Bruxellois"
            slogan="Pré-commande de Noël maintenant disponible! Réservez vite le vôtre !"
            fbCover={fbCover}
            linkToSample="#samples"
          >
            <CheckItem label="40 Flashs bruxellois en un livre unique" />
            <CheckItem label="illustrations en couleurs" />
            <CheckItem label="Préface par Dr. Ghada el Wakil" />
            <CheckItem label="Livre dédicacé par l'auteur" />
            <CheckItem label="Contenu inédit et petites surprises" />
          </RctBookPreviewComponent>
        </App>
      </Provider>
    </BrowserRouter>
  )))
