import { observable, action } from 'mobx'
import buildDebug from 'debug'

const debug = buildDebug('react-packages:packages:rct-data-table-component:column')

export default class Column {
  @observable name
  @observable enabled = false
  @observable type = 'string'

  constructor({ name, enabled, type }) {
    debug('constructor()')
    this.name = name
    this.enabled = enabled
    this.type = type
  }

  @action setEnabled(value) {
    debug('setEnabled()')
    this.enabled = value
  }
}
