import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { observer } from 'mobx-react'
import { observable, action } from 'mobx'
import Dialog, { DialogActions, DialogContent, DialogContentText, DialogTitle } from 'material-ui/Dialog'
import IconButton from 'material-ui/IconButton'
import Button from 'material-ui/Button'
import EditIcon from 'material-ui-icons/Edit'
import TextField from 'material-ui/TextField'
import Tooltip from 'material-ui/Tooltip'

import Column from './column'

const debug = buildDebug('react-packages:packages:rct-data-table-component:edit-dialog-component')

@observer
export default class EditDialogComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    columns: PropTypes.arrayOf(PropTypes.instanceOf(Column)).isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    item: PropTypes.object.isRequired,
    onSubmit: PropTypes.func.isRequired,
    title: PropTypes.string,
  }
  static defaultProps = {
    title: 'Edit',
  }

  componentWillMount() {
    debug('componentWillMount()')
    const { columns, item } = this.props
    this.reset()
    columns.forEach(({ name }) => {
      this.fields[name] = item[name]
    })
  }

  @action reset = () => {
    debug('reset()')
    const { columns } = this.props
    columns.forEach(({ name }) => {
      this.fields[name] = ''
    })
    this.open = false
  }
  @action handleOpen = () => {
    debug('handleOpen()')
    this.open = true
  }
  @action handleClose = () => {
    debug('handleClose()')
    this.open = false
  }
  @action handleInputChange = ({ target: { name, value } }) => {
    debug('handleInputChange()')
    this.fields[name] = value
  }
  @action handleOnSubmit = async (event) => {
    debug('handleOnSubmit()')
    event.preventDefault()
    const { onSubmit } = this.props
    await onSubmit(this.fields)
    this.reset()
  }

  @observable fields = {}
  @observable open = false

  render() {
    const { columns, title } = this.props
    return (
      <div>
        {/* add button */}
        <Tooltip title={title}>
          <IconButton onClick={this.handleOpen}>
            <EditIcon />
          </IconButton>
        </Tooltip>
        {/* edit form dialog */}
        <Dialog
          open={this.open}
          onRequestClose={this.handleClose}
        >
          <DialogTitle>Edit an item</DialogTitle>
          <DialogContent>
            <DialogContentText>
              You can edit the fields below and proceed with the button Update.
            </DialogContentText>
            { columns.map(({ name }) => (
              <TextField
                key={name}
                name={name}
                label={name}
                type="text"
                fullWidth
                defaultValue={this.fields[name]}
                onChange={this.handleInputChange}
              />
            ))}
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose}>Cancel</Button>
            <Button onClick={this.handleOnSubmit} raised color="primary">Update</Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}
