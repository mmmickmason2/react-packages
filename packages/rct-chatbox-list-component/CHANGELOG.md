# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.89.0"></a>
# [1.89.0](https://gitlab.com/4geit/react-packages/compare/v1.88.0...v1.89.0) (2017-11-29)




**Note:** Version bump only for package @4geit/rct-chatbox-list-component

<a name="1.87.5"></a>
## [1.87.5](https://gitlab.com/4geit/react-packages/compare/v1.87.4...v1.87.5) (2017-11-24)


### Bug Fixes

* **chatbox-list:** fix switch issue, pass correct userchatbox id to remove item, update the userchat ([5823c9d](https://gitlab.com/4geit/react-packages/commit/5823c9d))
* **components:** center data-table progress icon, add new class to override width right-side-menu ([bd644d8](https://gitlab.com/4geit/react-packages/commit/bd644d8))




<a name="1.86.2"></a>
## [1.86.2](https://gitlab.com/4geit/react-packages/compare/v1.86.1...v1.86.2) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-chatbox-list-component

<a name="1.86.0"></a>
# [1.86.0](https://gitlab.com/4geit/react-packages/compare/v1.85.3...v1.86.0) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-chatbox-list-component

<a name="1.85.3"></a>
## [1.85.3](https://gitlab.com/4geit/react-packages/compare/v1.85.2...v1.85.3) (2017-11-19)




**Note:** Version bump only for package @4geit/rct-chatbox-list-component

<a name="1.85.2"></a>
## [1.85.2](https://gitlab.com/4geit/react-packages/compare/v1.85.1...v1.85.2) (2017-11-18)




**Note:** Version bump only for package @4geit/rct-chatbox-list-component

<a name="1.83.3"></a>
## [1.83.3](https://gitlab.com/4geit/react-packages/compare/v1.83.2...v1.83.3) (2017-11-04)




**Note:** Version bump only for package @4geit/rct-chatbox-list-component

<a name="1.83.2"></a>
## [1.83.2](https://gitlab.com/4geit/react-packages/compare/v1.83.1...v1.83.2) (2017-11-04)




**Note:** Version bump only for package @4geit/rct-chatbox-list-component

<a name="1.83.1"></a>
## [1.83.1](https://gitlab.com/4geit/react-packages/compare/v1.83.0...v1.83.1) (2017-11-03)




**Note:** Version bump only for package @4geit/rct-chatbox-list-component

<a name="1.82.4"></a>
## [1.82.4](https://gitlab.com/4geit/react-packages/compare/v1.82.3...v1.82.4) (2017-11-02)




**Note:** Version bump only for package @4geit/rct-chatbox-list-component

<a name="1.81.2"></a>
## [1.81.2](https://gitlab.com/4geit/react-packages/compare/v1.81.1...v1.81.2) (2017-10-31)




**Note:** Version bump only for package @4geit/rct-chatbox-list-component

<a name="1.80.2"></a>
## [1.80.2](https://gitlab.com/4geit/react-packages/compare/v1.80.1...v1.80.2) (2017-10-29)




**Note:** Version bump only for package @4geit/rct-chatbox-list-component

<a name="1.79.0"></a>
# [1.79.0](https://gitlab.com/4geit/react-packages/compare/v1.78.2...v1.79.0) (2017-10-24)


### Bug Fixes

* **chatbox-list-component:** fix async/await issue ([80f5a56](https://gitlab.com/4geit/react-packages/commit/80f5a56))
* **rct-chatbox-list-component:** fix async method ([e8e1df5](https://gitlab.com/4geit/react-packages/commit/e8e1df5))


### Features

* **Chatbox List:** Switch State ([bf49921](https://gitlab.com/4geit/react-packages/commit/bf49921))




<a name="1.78.0"></a>
# [1.78.0](https://gitlab.com/4geit/react-packages/compare/v1.77.0...v1.78.0) (2017-10-21)


### Features

* **test:** add test environment with jest ([34a9b47](https://gitlab.com/4geit/react-packages/commit/34a9b47))




<a name="1.64.0"></a>
# [1.64.0](https://gitlab.com/4geit/react-packages/compare/v1.63.1...v1.64.0) (2017-10-06)


### Bug Fixes

* **chatbox list:** minor fix ([6f6b3e9](https://gitlab.com/4geit/react-packages/commit/6f6b3e9))
* **chatboxlist:** code fix ([0cc47be](https://gitlab.com/4geit/react-packages/commit/0cc47be))
* **chatboxlist:** code fix ([ab4d8d5](https://gitlab.com/4geit/react-packages/commit/ab4d8d5))
* **chatboxlist:** code fix ([77ba66e](https://gitlab.com/4geit/react-packages/commit/77ba66e))
* **chatboxlist:** code fix ([4b40f7f](https://gitlab.com/4geit/react-packages/commit/4b40f7f))
* **Chatboxlist:** switch state ([1a9361c](https://gitlab.com/4geit/react-packages/commit/1a9361c))




<a name="1.52.0"></a>
# [1.52.0](https://gitlab.com/4geit/react-packages/compare/v1.51.2...v1.52.0) (2017-10-04)


### Features

* **chatbox-grid:** add fetchMaximizedItem to store and use within component ([e1df4e4](https://gitlab.com/4geit/react-packages/commit/e1df4e4))




<a name="1.46.0"></a>
# [1.46.0](https://gitlab.com/4geit/react-packages/compare/v1.45.0...v1.46.0) (2017-10-03)


### Features

* **chatbox-list:** add switch logic add remove user chatbox ([8934788](https://gitlab.com/4geit/react-packages/commit/8934788))




<a name="1.35.0"></a>
# [1.35.0](https://gitlab.com/4geit/react-packages/compare/v1.34.4...v1.35.0) (2017-09-20)




**Note:** Version bump only for package @4geit/rct-chatbox-list-component

<a name="1.34.2"></a>
## [1.34.2](https://gitlab.com/4geit/react-packages/compare/v1.34.1...v1.34.2) (2017-09-20)




**Note:** Version bump only for package @4geit/rct-chatbox-list-component

<a name="1.34.0"></a>
# [1.34.0](https://gitlab.com/4geit/react-packages/compare/v1.33.0...v1.34.0) (2017-09-20)




**Note:** Version bump only for package @4geit/rct-chatbox-list-component

<a name="1.32.2"></a>
## [1.32.2](https://gitlab.com/4geit/react-packages/compare/v1.32.1...v1.32.2) (2017-09-18)




**Note:** Version bump only for package @4geit/rct-chatbox-list-component

<a name="1.26.0"></a>
# [1.26.0](https://gitlab.com/4geit/react-packages/compare/v1.24.1...v1.26.0) (2017-09-11)


### Features

* **chatbox-list:** add new component and store ([833bed3](https://gitlab.com/4geit/react-packages/commit/833bed3))




<a name="1.25.0"></a>
# [1.25.0](https://gitlab.com/4geit/react-packages/compare/v1.24.1...v1.25.0) (2017-09-11)


### Features

* **chatbox-list:** add new component and store ([833bed3](https://gitlab.com/4geit/react-packages/commit/833bed3))
