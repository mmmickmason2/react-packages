import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'mobx-react'
import { createShallow, createMount } from 'material-ui/test-utils'
import initStoryshots from '@storybook/addon-storyshots'
import buildDebug from 'debug'

// import xyzStore from '@4geit/rct-xyz-store'

import RctBookSampleComponent, { BookSample } from './rct-book-sample.component'

import sampleImage1 from './sampleImage1.jpg'
import sampleImage2 from './sampleImage2.jpg'
import sampleImage3 from './sampleImage3.jpg'
import sampleImage4 from './sampleImage4.jpg'
import sampleArticle1 from './sampleArticle1.jpg'
import sampleArticle2 from './sampleArticle2.jpg'
import sampleArticle3 from './sampleArticle3.jpg'

const debug = buildDebug('react-packages:packages:test:rct-book-sample-component')

const stores = {
  // xyzStore,
}

if (!process.env.DISABLE_STORYSHOTS) {
  initStoryshots({
    storyKindRegex: /^RctBookSampleComponent$/,
  })
}

const shallow = createShallow({ untilSelector: 'div' })
const mount = createMount()

it('renders without crashing', () => {
  debug('renders without crashing')
  const div = document.createElement('div')
  ReactDOM.render(
    <Provider {...stores}>
      <RctBookSampleComponent>
        <BookSample image={sampleImage1} article={sampleArticle1} />
        <BookSample image={sampleImage2} article={sampleArticle2} />
        <BookSample image={sampleImage3} article={sampleArticle3} />
        <BookSample image={sampleImage4} article={sampleArticle3} />
      </RctBookSampleComponent>
    </Provider>,
    div,
  )
})
// it('renders correctly', () => {
//   debug('renders correctly')
//   const wrapper = mount(<RctBookSampleComponent {...stores} />)
//   expect(wrapper).toMatchSnapshot()
// })
it('shallow-renders correctly', () => {
  debug('shallow-renders correctly')
  const wrapper = shallow((
    <RctBookSampleComponent {...stores}>
      <BookSample image={sampleImage1} article={sampleArticle1} />
      <BookSample image={sampleImage2} article={sampleArticle2} />
      <BookSample image={sampleImage3} article={sampleArticle3} />
      <BookSample image={sampleImage4} article={sampleArticle3} />
    </RctBookSampleComponent>
  ))
  expect(wrapper).toMatchSnapshot()
})
