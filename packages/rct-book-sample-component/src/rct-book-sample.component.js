import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
// eslint-disable-next-line no-unused-vars
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import Grid from 'material-ui/Grid'
import Card, { CardMedia } from 'material-ui/Card'

import './rct-book-sample.component.css'

const debug = buildDebug('react-packages:packages:rct-book-sample-component')

const cardStyle = {
  card: {
    height: 266,
    width: 400,
  },
}

export const BookSample = ({ image, article, invert }) => (
  <Grid item xs>
    <Grid container direction="row" spacing={24}>
      <Grid item xs>
        <Card style={cardStyle.card}>
          <CardMedia>
            <img src={!invert ? image : article} alt="" />
          </CardMedia>
        </Card>
      </Grid>
      <Grid item xs>
        <Card style={cardStyle.card}>
          <CardMedia>
            <img src={!invert ? article : image} alt="" />
          </CardMedia>
        </Card>
      </Grid>
    </Grid>
  </Grid>
)
BookSample.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  image: PropTypes.any.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  article: PropTypes.any.isRequired,
  invert: PropTypes.bool,
}
BookSample.defaultProps = {
  invert: false,
}

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  // root: {
  //   [theme.breakpoints.down('md')]: {
  //     width: '100%',
  //   },
  // },
  // TBD
}))
@withWidth()
// @inject('xyzStore')
@observer
export default class RctBookSampleComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.object.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types,
    children: PropTypes.any.isRequired,
  }
  static defaultProps = {
    // TBD
  }

  render() {
    debug('render()')
    const { children } = this.props

    return (
      <Grid container direction="column" alignItems="baseline">
        { children }
      </Grid>
    )
  }
}
