# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.87.5"></a>
## [1.87.5](https://gitlab.com/4geit/react-packages/compare/v1.87.4...v1.87.5) (2017-11-24)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.86.2"></a>
## [1.86.2](https://gitlab.com/4geit/react-packages/compare/v1.86.1...v1.86.2) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.86.0"></a>
# [1.86.0](https://gitlab.com/4geit/react-packages/compare/v1.85.3...v1.86.0) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.85.3"></a>
## [1.85.3](https://gitlab.com/4geit/react-packages/compare/v1.85.2...v1.85.3) (2017-11-19)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.85.2"></a>
## [1.85.2](https://gitlab.com/4geit/react-packages/compare/v1.85.1...v1.85.2) (2017-11-18)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.83.3"></a>
## [1.83.3](https://gitlab.com/4geit/react-packages/compare/v1.83.2...v1.83.3) (2017-11-04)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.83.1"></a>
## [1.83.1](https://gitlab.com/4geit/react-packages/compare/v1.83.0...v1.83.1) (2017-11-03)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.82.4"></a>
## [1.82.4](https://gitlab.com/4geit/react-packages/compare/v1.82.3...v1.82.4) (2017-11-02)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.80.2"></a>
## [1.80.2](https://gitlab.com/4geit/react-packages/compare/v1.80.1...v1.80.2) (2017-10-29)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.78.0"></a>
# [1.78.0](https://gitlab.com/4geit/react-packages/compare/v1.77.0...v1.78.0) (2017-10-21)


### Features

* **test:** add test environment with jest ([34a9b47](https://gitlab.com/4geit/react-packages/commit/34a9b47))




<a name="1.77.0"></a>
# [1.77.0](https://gitlab.com/4geit/react-packages/compare/v1.76.1...v1.77.0) (2017-10-21)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.75.0"></a>
# [1.75.0](https://gitlab.com/4geit/react-packages/compare/v1.74.1...v1.75.0) (2017-10-16)


### Bug Fixes

* **account:** minor fix ([6bc9ea3](https://gitlab.com/4geit/react-packages/commit/6bc9ea3))
* **account component:** minor fix ([2fc342c](https://gitlab.com/4geit/react-packages/commit/2fc342c))
* **account component:** minor fix ([03e2809](https://gitlab.com/4geit/react-packages/commit/03e2809))
* **account componnet:** minor fix ([0c73ef2](https://gitlab.com/4geit/react-packages/commit/0c73ef2))
* **account store:** minor fix ([0ba9547](https://gitlab.com/4geit/react-packages/commit/0ba9547))
* **account store:** minor fix ([64fae9e](https://gitlab.com/4geit/react-packages/commit/64fae9e))


### Features

* **Account Store:** create fetchData method to get user info ([461c460](https://gitlab.com/4geit/react-packages/commit/461c460))




<a name="1.71.0"></a>
# [1.71.0](https://gitlab.com/4geit/react-packages/compare/v1.70.2...v1.71.0) (2017-10-11)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.70.0"></a>
# [1.70.0](https://gitlab.com/4geit/react-packages/compare/v1.69.1...v1.70.0) (2017-10-10)


### Bug Fixes

* **header component:** added methods to authstore, commonstore and component ([48c3542](https://gitlab.com/4geit/react-packages/commit/48c3542))
* **header component:** minor fix ([af7c143](https://gitlab.com/4geit/react-packages/commit/af7c143))




<a name="1.35.0"></a>
# [1.35.0](https://gitlab.com/4geit/react-packages/compare/v1.34.4...v1.35.0) (2017-09-20)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.34.2"></a>
## [1.34.2](https://gitlab.com/4geit/react-packages/compare/v1.34.1...v1.34.2) (2017-09-20)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.34.0"></a>
# [1.34.0](https://gitlab.com/4geit/react-packages/compare/v1.33.0...v1.34.0) (2017-09-20)


### Features

* **swagger-client-store:** add swaggerUrl prop ([dbb173b](https://gitlab.com/4geit/react-packages/commit/dbb173b))




<a name="1.32.2"></a>
## [1.32.2](https://gitlab.com/4geit/react-packages/compare/v1.32.1...v1.32.2) (2017-09-18)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.24.1"></a>
## [1.24.1](https://gitlab.com/4geit/react-packages/compare/v1.24.0...v1.24.1) (2017-09-08)


### Bug Fixes

* **auth-store:** minor fix ([7d06e59](https://gitlab.com/4geit/react-packages/commit/7d06e59))




<a name="1.24.0"></a>
# [1.24.0](https://gitlab.com/4geit/react-packages/compare/v1.22.0...v1.24.0) (2017-09-08)


### Features

* **header:** add logout logic ([9689fc9](https://gitlab.com/4geit/react-packages/commit/9689fc9))




<a name="1.23.0"></a>
# [1.23.0](https://gitlab.com/4geit/react-packages/compare/v1.22.0...v1.23.0) (2017-09-08)


### Features

* **header:** add logout logic ([9689fc9](https://gitlab.com/4geit/react-packages/commit/9689fc9))




<a name="1.22.0"></a>
# [1.22.0](https://gitlab.com/4geit/react-packages/compare/v1.21.1...v1.22.0) (2017-09-08)


### Features

* **notification:** separate notification snackbar into a new component ([e528aba](https://gitlab.com/4geit/react-packages/commit/e528aba))




<a name="1.21.0"></a>
# [1.21.0](https://gitlab.com/4geit/react-packages/compare/v1.20.0...v1.21.0) (2017-09-08)


### Features

* **datatable:** add operationId and enabledColumns props and fetchData method ([760bf97](https://gitlab.com/4geit/react-packages/commit/760bf97))




<a name="1.8.0"></a>
# [1.8.0](https://gitlab.com/4geit/react-packages/compare/v1.7.2...v1.8.0) (2017-08-30)


### Features

* **layout:** add layout component jsx code ([904c8c6](https://gitlab.com/4geit/react-packages/commit/904c8c6))




<a name="1.7.2"></a>
## [1.7.2](https://gitlab.com/4geit/react-packages/compare/v1.7.1...v1.7.2) (2017-08-30)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.7.0"></a>
# [1.7.0](https://gitlab.com/4geit/react-packages/compare/v1.6.15...v1.7.0) (2017-08-29)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.6.15"></a>
## [1.6.15](https://gitlab.com/4geit/react-packages/compare/v1.6.14...v1.6.15) (2017-08-28)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.6.14"></a>
## [1.6.14](https://gitlab.com/4geit/react-packages/compare/v1.6.13...v1.6.14) (2017-08-28)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.6.13"></a>
## [1.6.13](https://gitlab.com/4geit/react-packages/compare/v1.6.12...v1.6.13) (2017-08-28)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.6.12"></a>
## [1.6.12](https://gitlab.com/4geit/react-packages/compare/v1.6.11...v1.6.12) (2017-08-28)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.6.11"></a>
## [1.6.11](https://gitlab.com/4geit/react-packages/compare/v1.6.10...v1.6.11) (2017-08-28)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.6.10"></a>
## [1.6.10](https://gitlab.com/4geit/react-packages/compare/v1.6.9...v1.6.10) (2017-08-27)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.6.9"></a>
## [1.6.9](https://gitlab.com/4geit/react-packages/compare/v1.6.8...v1.6.9) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.6.8"></a>
## [1.6.8](https://gitlab.com/4geit/react-packages/compare/v1.6.7...v1.6.8) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.6.7"></a>
## [1.6.7](https://gitlab.com/4geit/react-packages/compare/v1.6.6...v1.6.7) (2017-08-26)


### Bug Fixes

* **template:** fix minor issue ([a290cb9](https://gitlab.com/4geit/react-packages/commit/a290cb9))




<a name="1.6.6"></a>
## [1.6.6](https://gitlab.com/4geit/react-packages/compare/v1.6.5...v1.6.6) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.6.5"></a>
## [1.6.5](https://gitlab.com/4geit/react-packages/compare/v1.6.4...v1.6.5) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-auth-store

<a name="1.6.4"></a>
## [1.6.4](https://gitlab.com/4geit/react-packages/compare/1.6.3...1.6.4) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-auth-store
