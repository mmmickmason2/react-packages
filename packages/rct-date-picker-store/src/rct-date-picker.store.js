import { observable, action } from 'mobx'
import buildDebug from 'debug'

const debug = buildDebug('react-packages:packages:rct-date-picker-store')

export class RctDatePickerStore {
  @observable date = null
  @observable focused = false

  @action setDate(value) {
    debug('setDate()')
    this.date = value
  }
  @action setFocused(value) {
    debug('setFocused()')
    this.focused = value
  }
}

export default new RctDatePickerStore()
