# @4geit/rct-reorderable-grid-list-component [![npm version](//badge.fury.io/js/@4geit%2Frct-reorderable-grid-list-component.svg)](//badge.fury.io/js/@4geit%2Frct-reorderable-grid-list-component)

---

reorderable grid list using react-dnd

## Demo

A live storybook is available to see how the component looks like @ http://react-packages.ws3.4ge.it

## Installation

1. A recommended way to install ***@4geit/rct-reorderable-grid-list-component*** is through [npm](//www.npmjs.com/search?q=@4geit/rct-reorderable-grid-list-component) package manager using the following command:

```bash
npm i @4geit/rct-reorderable-grid-list-component --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/rct-reorderable-grid-list-component
```

2. Depending on where you want to use the component you will need to import the class `RctReorderableGridListComponent` to your project JS file as follows:

```js
import RctReorderableGridListComponent from '@4geit/rct-reorderable-grid-list-component'
```

For instance if you want to use this component in your `App.js` component, you can use the RctReorderableGridListComponent component in the JSX code as follows:

```js
import React from 'react'
// ...
import RctReorderableGridListComponent from '@4geit/rct-reorderable-grid-list-component'
// ...
const WrapReorderableItemComponent = props => (
  <div>{ props.itemId }</div>
)
// ...
const App = () => (
  <div className="App">
    <RctReorderableGridListComponent
      cols={ 3 }
      handleSetPosition={ () => {} }
      fetchData={ () => {} }
      data={ [] }
      itemComponent={ <WrapReorderableItemComponent/> }
    />
  </div>
)
```
