import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Provider, inject, observer } from 'mobx-react'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'

import commonStore from '@4geit/rct-common-store'

import RctReorderableGridListComponent from './rct-reorderable-grid-list.component'

const stores = {
  commonStore,
}

/* eslint-disable no-unused-vars */
const WrapReorderableItemComponent = (
  <div />
)

@inject('reorderableGridListStore')
@observer
class WrapReorderableGridListComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    reorderableGridListStore: PropTypes.any.isRequired,
  }

  render() {
    const { reorderableGridListStore } = this.props
    const { fetchData, setPosition, sortedData } = reorderableGridListStore
    /* eslint-disable react/jsx-no-bind */
    return (
      <RctReorderableGridListComponent
        cols={3}
        handleSetPosition={setPosition.bind(reorderableGridListStore)}
        handleFetchData={fetchData.bind(reorderableGridListStore)}
        data={sortedData}
        itemComponent={<WrapReorderableItemComponent />}
      />
    )
    /* eslint-enable react/jsx-no-bind */
  }
}
/* eslint-enable no-unused-vars */

storiesOf('RctReorderableGridListComponent', module)
  .add('simple usage', withInfo()(() => (
    <Provider {...stores}>
      <div>Test</div>
      {/* <WrapReorderableGridListComponent/> */}
    </Provider>
  )))
