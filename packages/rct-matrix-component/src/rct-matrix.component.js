/* eslint-disable react/no-array-index-key */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
// eslint-disable-next-line no-unused-vars
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'

import RctCellComponent from '@4geit/rct-cell-component'

import './rct-matrix.component.css'

const debug = buildDebug('react-packages:packages:rct-matrix-component')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  // root: {
  //   [theme.breakpoints.down('md')]: {
  //     width: '100%',
  //   },
  // },
  // TBD
}))
@withWidth()
// @inject('xyzStore')
@observer
export default class RctMatrixComponent extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.object.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    nrows: PropTypes.number,
    ncols: PropTypes.number,
    board: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.bool)).isRequired,
  }
  static defaultProps = {
    nrows: 25,
    ncols: 40,
    // TBD
  }

  render() {
    debug('render()')
    const { nrows, ncols, board } = this.props
    return (
      <svg
        style={{
          width: ncols * 10,
          height: nrows * 10,
        }}
      >
        { board.map((row, y) => (
          row.map((col, x) => (
            <RctCellComponent
              key={`${y}:${x}`}
              row={y}
              col={x}
              filled={col}
            />
          ))
        )) }
      </svg>
    )
  }
}
