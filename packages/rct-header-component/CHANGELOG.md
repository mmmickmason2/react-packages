# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.92.0"></a>
# [1.92.0](https://gitlab.com/4geit/react-packages/compare/v1.91.0...v1.92.0) (2017-12-01)


### Features

* **search-input-component:** add autocompletion ([a16cb32](https://gitlab.com/4geit/react-packages/commit/a16cb32))




<a name="1.89.1"></a>
## [1.89.1](https://gitlab.com/4geit/react-packages/compare/v1.89.0...v1.89.1) (2017-11-29)




**Note:** Version bump only for package @4geit/rct-header-component

<a name="1.89.0"></a>
# [1.89.0](https://gitlab.com/4geit/react-packages/compare/v1.88.0...v1.89.0) (2017-11-29)


### Bug Fixes

* **chatbox-grid:** improve maximize page ([b0b1528](https://gitlab.com/4geit/react-packages/commit/b0b1528))




<a name="1.87.5"></a>
## [1.87.5](https://gitlab.com/4geit/react-packages/compare/v1.87.4...v1.87.5) (2017-11-24)




**Note:** Version bump only for package @4geit/rct-header-component

<a name="1.87.1"></a>
## [1.87.1](https://gitlab.com/4geit/react-packages/compare/v1.87.0...v1.87.1) (2017-11-23)




**Note:** Version bump only for package @4geit/rct-header-component

<a name="1.86.3"></a>
## [1.86.3](https://gitlab.com/4geit/react-packages/compare/v1.86.2...v1.86.3) (2017-11-21)




**Note:** Version bump only for package @4geit/rct-header-component

<a name="1.86.2"></a>
## [1.86.2](https://gitlab.com/4geit/react-packages/compare/v1.86.1...v1.86.2) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-header-component

<a name="1.86.0"></a>
# [1.86.0](https://gitlab.com/4geit/react-packages/compare/v1.85.3...v1.86.0) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-header-component

<a name="1.85.3"></a>
## [1.85.3](https://gitlab.com/4geit/react-packages/compare/v1.85.2...v1.85.3) (2017-11-19)




**Note:** Version bump only for package @4geit/rct-header-component

<a name="1.85.2"></a>
## [1.85.2](https://gitlab.com/4geit/react-packages/compare/v1.85.1...v1.85.2) (2017-11-18)




**Note:** Version bump only for package @4geit/rct-header-component

<a name="1.84.2"></a>
## [1.84.2](https://gitlab.com/4geit/react-packages/compare/v1.84.1...v1.84.2) (2017-11-15)




**Note:** Version bump only for package @4geit/rct-header-component

<a name="1.83.5"></a>
## [1.83.5](https://gitlab.com/4geit/react-packages/compare/v1.83.4...v1.83.5) (2017-11-06)




**Note:** Version bump only for package @4geit/rct-header-component

<a name="1.83.3"></a>
## [1.83.3](https://gitlab.com/4geit/react-packages/compare/v1.83.2...v1.83.3) (2017-11-04)




**Note:** Version bump only for package @4geit/rct-header-component

<a name="1.83.2"></a>
## [1.83.2](https://gitlab.com/4geit/react-packages/compare/v1.83.1...v1.83.2) (2017-11-04)




**Note:** Version bump only for package @4geit/rct-header-component

<a name="1.82.4"></a>
## [1.82.4](https://gitlab.com/4geit/react-packages/compare/v1.82.3...v1.82.4) (2017-11-02)




**Note:** Version bump only for package @4geit/rct-header-component

<a name="1.82.1"></a>
## [1.82.1](https://gitlab.com/4geit/react-packages/compare/v1.82.0...v1.82.1) (2017-11-01)




**Note:** Version bump only for package @4geit/rct-header-component

<a name="1.81.2"></a>
## [1.81.2](https://gitlab.com/4geit/react-packages/compare/v1.81.1...v1.81.2) (2017-10-31)




**Note:** Version bump only for package @4geit/rct-header-component

<a name="1.81.0"></a>
# [1.81.0](https://gitlab.com/4geit/react-packages/compare/v1.80.5...v1.81.0) (2017-10-31)


### Bug Fixes

* **header:** minor fix ([0d838d4](https://gitlab.com/4geit/react-packages/commit/0d838d4))


### Features

* **header component:** hide search imput if user unlogged ([b74c91e](https://gitlab.com/4geit/react-packages/commit/b74c91e))




<a name="1.80.5"></a>
## [1.80.5](https://gitlab.com/4geit/react-packages/compare/v1.80.4...v1.80.5) (2017-10-30)




**Note:** Version bump only for package @4geit/rct-header-component

<a name="1.80.2"></a>
## [1.80.2](https://gitlab.com/4geit/react-packages/compare/v1.80.1...v1.80.2) (2017-10-29)




**Note:** Version bump only for package @4geit/rct-header-component

<a name="1.78.0"></a>
# [1.78.0](https://gitlab.com/4geit/react-packages/compare/v1.77.0...v1.78.0) (2017-10-21)


### Features

* **test:** add test environment with jest ([34a9b47](https://gitlab.com/4geit/react-packages/commit/34a9b47))




<a name="1.77.0"></a>
# [1.77.0](https://gitlab.com/4geit/react-packages/compare/v1.76.1...v1.77.0) (2017-10-21)




**Note:** Version bump only for package @4geit/rct-header-component

<a name="1.75.1"></a>
## [1.75.1](https://gitlab.com/4geit/react-packages/compare/v1.75.0...v1.75.1) (2017-10-16)


### Bug Fixes

* **upgrade:** upgrade to last version of MUI ([91a14ab](https://gitlab.com/4geit/react-packages/commit/91a14ab))




<a name="1.75.0"></a>
# [1.75.0](https://gitlab.com/4geit/react-packages/compare/v1.74.1...v1.75.0) (2017-10-16)




**Note:** Version bump only for package @4geit/rct-header-component

<a name="1.71.0"></a>
# [1.71.0](https://gitlab.com/4geit/react-packages/compare/v1.70.2...v1.71.0) (2017-10-11)


### Bug Fixes

* **header component and common store:** minor fix ([b2885a3](https://gitlab.com/4geit/react-packages/commit/b2885a3))


### Features

* **header component:** added style ([7289215](https://gitlab.com/4geit/react-packages/commit/7289215))




<a name="1.70.0"></a>
# [1.70.0](https://gitlab.com/4geit/react-packages/compare/v1.69.1...v1.70.0) (2017-10-10)


### Bug Fixes

* **header component:** added methods to authstore, commonstore and component ([48c3542](https://gitlab.com/4geit/react-packages/commit/48c3542))
* **header component:** code fix ([ebc2492](https://gitlab.com/4geit/react-packages/commit/ebc2492))
* **header component:** minor fix ([af7c143](https://gitlab.com/4geit/react-packages/commit/af7c143))


### Features

* **Header component:** Add user firstname and lastname when logged in ([ae64b25](https://gitlab.com/4geit/react-packages/commit/ae64b25))




<a name="1.67.0"></a>
# [1.67.0](https://gitlab.com/4geit/react-packages/compare/v1.66.0...v1.67.0) (2017-10-09)


### Bug Fixes

* **header component:** minor fix ([2992ae4](https://gitlab.com/4geit/react-packages/commit/2992ae4))


### Features

* **header component:** change account button when logged in with the link to account` section ([f3f7f80](https://gitlab.com/4geit/react-packages/commit/f3f7f80))




<a name="1.52.0"></a>
# [1.52.0](https://gitlab.com/4geit/react-packages/compare/v1.51.2...v1.52.0) (2017-10-04)


### Features

* **chatbox-grid:** add fetchMaximizedItem to store and use within component ([e1df4e4](https://gitlab.com/4geit/react-packages/commit/e1df4e4))




<a name="1.46.0"></a>
# [1.46.0](https://gitlab.com/4geit/react-packages/compare/v1.45.0...v1.46.0) (2017-10-03)




**Note:** Version bump only for package @4geit/rct-header-component

<a name="1.32.2"></a>
## [1.32.2](https://gitlab.com/4geit/react-packages/compare/v1.32.1...v1.32.2) (2017-09-18)




**Note:** Version bump only for package @4geit/rct-header-component

<a name="1.24.0"></a>
# [1.24.0](https://gitlab.com/4geit/react-packages/compare/v1.22.0...v1.24.0) (2017-09-08)


### Features

* **header:** add logout logic ([9689fc9](https://gitlab.com/4geit/react-packages/commit/9689fc9))




<a name="1.23.0"></a>
# [1.23.0](https://gitlab.com/4geit/react-packages/compare/v1.22.0...v1.23.0) (2017-09-08)


### Features

* **header:** add logout logic ([9689fc9](https://gitlab.com/4geit/react-packages/commit/9689fc9))




<a name="1.18.0"></a>
# [1.18.0](https://gitlab.com/4geit/react-packages/compare/v1.17.1...v1.18.0) (2017-09-08)


### Bug Fixes

* **header:** code review ([aa3ecb7](https://gitlab.com/4geit/react-packages/commit/aa3ecb7))
* **header:** logout icon ([7697d0c](https://gitlab.com/4geit/react-packages/commit/7697d0c))
* **header:** minor fix ([81ec47f](https://gitlab.com/4geit/react-packages/commit/81ec47f))


### Features

* **Header component:** Add logout icon ([a188284](https://gitlab.com/4geit/react-packages/commit/a188284))




<a name="1.17.1"></a>
## [1.17.1](https://gitlab.com/4geit/react-packages/compare/v1.12.0...v1.17.1) (2017-09-07)




**Note:** Version bump only for package @4geit/rct-header-component

<a name="1.17.0"></a>
# [1.17.0](https://gitlab.com/4geit/react-packages/compare/v1.16.1...v1.17.0) (2017-09-07)


### Bug Fixes

* **header-component:** minor change ([6260a37](https://gitlab.com/4geit/react-packages/commit/6260a37))
* **layout:** fix sidemenu display in layout component, remove overflow, fix toolbar elements display ([539f856](https://gitlab.com/4geit/react-packages/commit/539f856))
* **layout:** remove toolbar from header, add header content to sidemenu component and finally use it ([12ca4a6](https://gitlab.com/4geit/react-packages/commit/12ca4a6))


### Features

* **layout:** add link to side-menu items and login button in header ([60ef6c6](https://gitlab.com/4geit/react-packages/commit/60ef6c6))




<a name="1.16.0"></a>
# [1.16.0](https://gitlab.com/4geit/react-packages/compare/v1.16.1...v1.16.0) (2017-09-07)


### Bug Fixes

* **header-component:** minor change ([6260a37](https://gitlab.com/4geit/react-packages/commit/6260a37))
* **layout:** fix sidemenu display in layout component, remove overflow, fix toolbar elements display ([539f856](https://gitlab.com/4geit/react-packages/commit/539f856))
* **layout:** remove toolbar from header, add header content to sidemenu component and finally use it ([12ca4a6](https://gitlab.com/4geit/react-packages/commit/12ca4a6))


### Features

* **layout:** add link to side-menu items and login button in header ([60ef6c6](https://gitlab.com/4geit/react-packages/commit/60ef6c6))




<a name="1.15.4"></a>
## [1.15.4](https://gitlab.com/4geit/react-packages/compare/v1.15.2...v1.15.4) (2017-09-06)


### Bug Fixes

* **project:** remove deprecated prop, add header component dep ([1d07e3e](https://gitlab.com/4geit/react-packages/commit/1d07e3e))




<a name="1.15.3"></a>
## [1.15.3](https://gitlab.com/4geit/react-packages/compare/v1.15.2...v1.15.3) (2017-09-05)


### Bug Fixes

* **project:** remove deprecated prop, add header component dep ([1d07e3e](https://gitlab.com/4geit/react-packages/commit/1d07e3e))




<a name="1.12.0"></a>
# [1.12.0](https://gitlab.com/4geit/react-packages/compare/v1.11.0...v1.12.0) (2017-09-02)


### Features

* **header:** add search bar ([c799a59](https://gitlab.com/4geit/react-packages/commit/c799a59))




<a name="1.11.0"></a>
# [1.11.0](https://gitlab.com/4geit/react-packages/compare/v1.9.0...v1.11.0) (2017-09-01)


### Features

* **header:** add search bar ([fad09f0](https://gitlab.com/4geit/react-packages/commit/fad09f0))
* **scripts:** add styling code base to components ([e797fcc](https://gitlab.com/4geit/react-packages/commit/e797fcc))




<a name="1.9.0"></a>
# [1.9.0](https://gitlab.com/4geit/react-packages/compare/v1.8.0...v1.9.0) (2017-08-31)


### Features

* **header:** add new component + upgrade MUI and fix breaking changes ([8ac2ee5](https://gitlab.com/4geit/react-packages/commit/8ac2ee5))
