import React from 'react'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'

import RctFooterComponent, { FooterItem } from './rct-footer.component'

storiesOf('RctFooterComponent', module)
  .add('simple usage', withInfo()(() => (
    <RctFooterComponent>
      <FooterItem icon="restore" label="Recents" />
      <FooterItem icon="favorite" label="Favorites" />
    </RctFooterComponent>
  )))
