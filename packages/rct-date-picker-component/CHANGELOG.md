# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.87.5"></a>
## [1.87.5](https://gitlab.com/4geit/react-packages/compare/v1.87.4...v1.87.5) (2017-11-24)




**Note:** Version bump only for package @4geit/rct-date-picker-component

<a name="1.87.1"></a>
## [1.87.1](https://gitlab.com/4geit/react-packages/compare/v1.87.0...v1.87.1) (2017-11-23)


### Bug Fixes

* **rct-notification-menu-component:** reset tinyicon when notifications are read ([491f0d4](https://gitlab.com/4geit/react-packages/commit/491f0d4))




<a name="1.86.2"></a>
## [1.86.2](https://gitlab.com/4geit/react-packages/compare/v1.86.1...v1.86.2) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-date-picker-component

<a name="1.86.0"></a>
# [1.86.0](https://gitlab.com/4geit/react-packages/compare/v1.85.3...v1.86.0) (2017-11-20)




**Note:** Version bump only for package @4geit/rct-date-picker-component

<a name="1.85.3"></a>
## [1.85.3](https://gitlab.com/4geit/react-packages/compare/v1.85.2...v1.85.3) (2017-11-19)




**Note:** Version bump only for package @4geit/rct-date-picker-component

<a name="1.85.2"></a>
## [1.85.2](https://gitlab.com/4geit/react-packages/compare/v1.85.1...v1.85.2) (2017-11-18)




**Note:** Version bump only for package @4geit/rct-date-picker-component

<a name="1.83.3"></a>
## [1.83.3](https://gitlab.com/4geit/react-packages/compare/v1.83.2...v1.83.3) (2017-11-04)




**Note:** Version bump only for package @4geit/rct-date-picker-component

<a name="1.83.2"></a>
## [1.83.2](https://gitlab.com/4geit/react-packages/compare/v1.83.1...v1.83.2) (2017-11-04)




**Note:** Version bump only for package @4geit/rct-date-picker-component

<a name="1.82.4"></a>
## [1.82.4](https://gitlab.com/4geit/react-packages/compare/v1.82.3...v1.82.4) (2017-11-02)




**Note:** Version bump only for package @4geit/rct-date-picker-component

<a name="1.81.2"></a>
## [1.81.2](https://gitlab.com/4geit/react-packages/compare/v1.81.1...v1.81.2) (2017-10-31)




**Note:** Version bump only for package @4geit/rct-date-picker-component

<a name="1.80.2"></a>
## [1.80.2](https://gitlab.com/4geit/react-packages/compare/v1.80.1...v1.80.2) (2017-10-29)




**Note:** Version bump only for package @4geit/rct-date-picker-component

<a name="1.78.0"></a>
# [1.78.0](https://gitlab.com/4geit/react-packages/compare/v1.77.0...v1.78.0) (2017-10-21)


### Features

* **test:** add test environment with jest ([34a9b47](https://gitlab.com/4geit/react-packages/commit/34a9b47))




<a name="1.70.1"></a>
## [1.70.1](https://gitlab.com/4geit/react-packages/compare/v1.70.0...v1.70.1) (2017-10-10)




**Note:** Version bump only for package @4geit/rct-date-picker-component

<a name="1.52.0"></a>
# [1.52.0](https://gitlab.com/4geit/react-packages/compare/v1.51.2...v1.52.0) (2017-10-04)


### Features

* **chatbox-grid:** add fetchMaximizedItem to store and use within component ([e1df4e4](https://gitlab.com/4geit/react-packages/commit/e1df4e4))




<a name="1.46.0"></a>
# [1.46.0](https://gitlab.com/4geit/react-packages/compare/v1.45.0...v1.46.0) (2017-10-03)




**Note:** Version bump only for package @4geit/rct-date-picker-component

<a name="1.39.4"></a>
## [1.39.4](https://gitlab.com/4geit/react-packages/compare/v1.39.3...v1.39.4) (2017-09-21)




**Note:** Version bump only for package @4geit/rct-date-picker-component

<a name="1.39.2"></a>
## [1.39.2](https://gitlab.com/4geit/react-packages/compare/v1.39.1...v1.39.2) (2017-09-21)


### Bug Fixes

* **date-picker:** fix proper use of action method of the store ([d90f3d2](https://gitlab.com/4geit/react-packages/commit/d90f3d2))
* **date-picker-component:** fix issue with observable assignment ([5ddf8f4](https://gitlab.com/4geit/react-packages/commit/5ddf8f4))




<a name="1.39.0"></a>
# [1.39.0](https://gitlab.com/4geit/react-packages/compare/v1.38.0...v1.39.0) (2017-09-21)


### Bug Fixes

* **date-picker-component:** remove button and add it to eventListComponent ([d474352](https://gitlab.com/4geit/react-packages/commit/d474352))
* **date-picker-container:** add state and search button ([203155f](https://gitlab.com/4geit/react-packages/commit/203155f))
* **minor:** minor ([0e2f966](https://gitlab.com/4geit/react-packages/commit/0e2f966))


### Features

* **date-picker-component:** add the single date picker component ([697c112](https://gitlab.com/4geit/react-packages/commit/697c112))
