import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { inject, observer } from 'mobx-react'

import TextField from 'material-ui/TextField'
import Checkbox from 'material-ui/Checkbox'
import Card, { CardHeader, CardContent, CardActions } from 'material-ui/Card'
import VpnKeyIcon from 'material-ui-icons/VpnKey'
import Button from 'material-ui/Button'
import Grid from 'material-ui/Grid'
import Typography from 'material-ui/Typography'
import { FormControlLabel } from 'material-ui/Form'
import { withRouter } from 'react-router-dom'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'

import { RctAuthStore } from '@4geit/rct-auth-store'

import './rct-login.component.css'

const debug = buildDebug('react-packages:packages:rct-login-component')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  // TBD
}))
@withWidth()
@inject('authStore', 'notificationStore')
@withRouter
@observer
export default class RctLoginComponent extends Component {
  static propTypes = {
    authStore: PropTypes.instanceOf(RctAuthStore).isRequired,
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.any.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    history: PropTypes.object.isRequired,
    cardWidth: PropTypes.string,
    redirectTo: PropTypes.string,
  }
  static defaultProps = {
    cardWidth: '100%',
    redirectTo: '/',
  }

  componentWillUnmount() {
    debug('componentWillUnmount()')
    this.props.authStore.reset()
  }
  handleEmailChange = (event) => {
    debug('handleEmailChange()')
    this.props.authStore.setEmail(event.target.value)
  }
  handlePasswordChange = (event) => {
    debug('handlePasswordChange()')
    this.props.authStore.setPassword(event.target.value)
  }
  handleRememberChange = (event, isInputChecked) => {
    debug('handleRememberChange()')
    this.props.authStore.setRemember(isInputChecked)
  }
  handleSubmitForm = async (event) => {
    debug('handleSubmitForm()')
    event.preventDefault()
    await this.props.authStore.login()
    this.props.history.replace(this.props.redirectTo)
  }
  render() {
    debug('render()')
    const { cardWidth } = this.props
    const { values, inProgress } = this.props.authStore
    return (
      <div style={{ width: cardWidth }} >
        <form onSubmit={this.handleSubmitForm}>
          <Card>
            <CardHeader
              title="Login"
              subheader="Get connected to your account"
              avatar={<VpnKeyIcon />}
            />
            <CardContent>
              <Typography type="body1" gutterBottom>
                Fill out the fields above with your credentials.
              </Typography>
              <div>
                <TextField
                  label="Email"
                  type="email"
                  id="email"
                  name="email"
                  value={values.email}
                  onChange={this.handleEmailChange}
                  fullWidth
                  required
                  autoFocus
                />
              </div>
              <div>
                <TextField
                  label="Password"
                  type="password"
                  id="password"
                  name="password"
                  value={values.password}
                  onChange={this.handlePasswordChange}
                  fullWidth
                  required
                />
              </div>
              <div>
                <FormControlLabel
                  control={
                    <Checkbox
                      id="remember"
                      name="remember"
                      checked={values.remember}
                      onChange={this.handleRememberChange}
                    />
                  }
                  label="Remember me"
                />
              </div>
            </CardContent>
            <CardActions>
              <Grid container justify="center">
                <Grid item>
                  <Button
                    raised
                    color="accent"
                    type="submit"
                    disabled={inProgress}
                  >
                    Sign in
                  </Button>
                </Grid>
              </Grid>
            </CardActions>
          </Card>
        </form>
      </div>
    )
  }
}
