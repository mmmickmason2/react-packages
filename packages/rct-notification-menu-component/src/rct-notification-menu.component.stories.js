import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Provider, inject, observer } from 'mobx-react'
import { BrowserRouter } from 'react-router-dom'
import { storiesOf } from '@storybook/react'
import centered from '@storybook/addon-centered'
import { withInfo } from '@storybook/addon-info'
import IconButton from 'material-ui/IconButton'
import Icon from 'material-ui/Icon'
import Menu, { MenuItem } from 'material-ui/Menu'

import commonStore from '@4geit/rct-common-store'
import swaggerClientStore from '@4geit/rct-swagger-client-store'
import notificationMenuStore from '@4geit/rct-notification-menu-store'

import RctNotificationMenuComponent from './rct-notification-menu.component'

const stores = {
  commonStore,
  swaggerClientStore,
  notificationMenuStore,
}

@inject('swaggerClientStore', 'commonStore')
@observer
class App extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    children: PropTypes.any.isRequired,
  }

  async componentWillMount() {
    try {
      await swaggerClientStore.buildClient({
        apiUrl: process.env.STORYBOOK_API_URL || 'http://localhost:10010/v1',
      })
      await swaggerClientStore.buildClientWithToken({
        token: process.env.STORYBOOK_TOKEN,
      })
      const {
        body: {
          token, _id, id, ...fields
        },
      } = await swaggerClientStore.client.apis.Account.account()
      commonStore.setToken(token)
      commonStore.setUser({ ...fields })
      commonStore.setAppLoaded()
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err)
    }
  }

  render() {
    const { children } = this.props
    if (commonStore.appLoaded) {
      return (
        <div>
          { children }
        </div>
      )
    }
    return (
      <div>Loading...</div>
    )
  }
}

/* eslint-disable react/no-multi-comp */
class TestComponent extends Component {
  state = {
    open: false,
    anchorEl: null,
    options: [
      'aaaa',
      'bbbb',
      'cccc',
      'dddd',
    ],
  }
  render() {
    return (
      <div>
        <IconButton onClick={event => this.setState({ open: true, anchorEl: event.currentTarget })}>
          <Icon>notifications</Icon>
        </IconButton>
        <Menu
          id="lock-menu"
          anchorEl={this.state.anchorEl}
          open={this.state.open}
          onRequestClose={() => this.setState({ open: false })}
        >
          <MenuItem selected={false}>test</MenuItem>
          {this.state.options.map((option, index) => (
            <MenuItem key={option} selected={index === 0}>{option}</MenuItem>
          ))}
        </Menu>
      </div>
    )
  }
}
/* eslint-enable react/no-multi-comp */

storiesOf('RctNotificationMenuComponent', module)
  .addDecorator(centered)
  .add('simple usage', withInfo()(() => (
    <BrowserRouter>
      <Provider {...stores}>
        <App>
          <RctNotificationMenuComponent />
        </App>
      </Provider>
    </BrowserRouter>
  )))
  .add('selected menu', withInfo()(() => (
    <TestComponent />
  )))
