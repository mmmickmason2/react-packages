// import debug
import buildDebug from 'debug'
// prepare debug object
const debug = buildDebug('react-packages:__mocks__:rct-common-store')
// we generate an manualy mocked object
const rctCommonStore = jest.genMockFromModule('@4geit/rct-common-store')

// mocking members
rctCommonStore.token = '123467890'
rctCommonStore.user = {
  _id: 0,
  id: 'xxx-yyy-zzz',
  email: 'aaaa@bbbb.xyz',
  password: 'passw0rd',
  token: '123467890',
  firstname: 'aaaa',
  lastname: 'bbbb',
  company: 'my company',
  address: {
    street: 'first street',
    city: 'betterland',
    state: 'poney',
    postcode: '1234',
    country: 'unicorn',
  },
  phone: '1111-222-333',
}
rctCommonStore.isLoggedIn = true
rctCommonStore.appLoaded = true

// mocking methods
rctCommonStore.setToken = async () => {
  debug('setToken mocked!')
}
rctCommonStore.setAppLoaded = async () => {
  debug('setAppLoaded mocked!')
}
rctCommonStore.logout = async () => {
  debug('logout mocked!')
}
rctCommonStore.setUser = async () => {
  debug('setUser mocked!')
}
rctCommonStore.restoreSession = async () => {
  debug('restoreSession mocked!')
}

export default rctCommonStore
