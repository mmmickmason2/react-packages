// This file is used by storybook in order to override it's webpack
// configuration.

// we load the storybook webpack config generator function
const genDefaultConfig = require('@storybook/react/dist/server/config/defaults/webpack.config.js')
// we need dotenv-webpack to load some environment variables from .env files
const Dotenv = require('dotenv-webpack')
// path is used to resolve paths
const path = require('path')

module.exports = (baseConfig, env) => {
  // we generate the webpack config object here thanks to the storybook
  // generator
  const config = genDefaultConfig(baseConfig, env)
  // we destructure some fields we need to update from the config object
  const { plugins } = config
  // change `mainFields` to `src:module` as an entrypoint since the packages
  // uses `src:module`
  config.resolve = {
    mainFields: ['src:module', 'browser', 'module', 'main'],
  }
  // we load the env variables from .env.local
  plugins.push(new Dotenv({ path: path.resolve('.env.local') }))
  // we load the env variables from .env, the variables that have been already
  // defined are skipped
  plugins.push(new Dotenv({ path: path.resolve('.env') }))
  // we return the config object used by the storybook webpack setup
  return config
}
